package com.devcamp.task5810api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5810api.model.CVoucher;
import com.devcamp.task5810api.repository.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {
    @Autowired
    IVoucherRepository pIVoucherRepository;

    @GetMapping("/voucher")
   public ResponseEntity<List<CVoucher>> getVouchergetVoucherList() {
       try {
           List<CVoucher>  list = new ArrayList<CVoucher>();
           pIVoucherRepository.findAll().forEach(list::add);
           return new ResponseEntity<>(list,HttpStatus.OK);
       } catch (Exception e) {
           return null;
       }
      
   }
   @GetMapping("/voucher5")
   public ResponseEntity<List<CVoucher>> getFiveVoucher(
                       @RequestParam(value = "page", defaultValue = "1") String page,
                       @RequestParam(value = "size", defaultValue = "5") String size) {
       try {
           PageRequest pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
           List<CVoucher>  list = new ArrayList<CVoucher>();
           pIVoucherRepository.findAll(pageWithFiveElements).forEach(list::add);
           return new ResponseEntity<>(list,HttpStatus.OK);
       } catch (Exception e) {
           return null; 
       }
   } 


}
